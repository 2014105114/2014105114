#include <stdio.h>



precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n) ++];
	switch(*symbol){
	case '(' : return lparen;
	case ')' : return rparen;
	case '+' : return plus;
	case '-' : return minus;
	case '/' : return divide;
	case '*' : return times;
	case '%' : return mod;
	case '\0' : return eos;
	default : return operand;

	}
}

void main()
{
	FILE* fpIn;
	FILE* fpOut;
	char symbol;
	precedence token;
	int n = 0;
	top = 0;
	stack[0] = eos;

	fopen_s(&fpIn, "infix.txt", "r");
	fopen_s(&fpOut, "postfix.txt", "w");

	for(token == getToken(&symbol, &n) ; token != eos; token = getToken(&symbol, &n)) {
		if(token == operand)
			printf("%c", symbol);
		else if(token == rparen) {
			while (stack[top] != lparen)
				printToken(pop());
			pop();
		}
		else{
			while(isp[stack[top]] >= icp[token])
				printToken(pop());
			push(token);
		}
	}
	while((token = pop()) != eos)
		printToken(token);
	printf("\n");


	fclose(fpIn);
	fclose(fpOut);
}