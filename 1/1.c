#include <stdio.h>
#define MAX_TERMS 20

typedef struct {
	int row;
	int col;

	int r;
	int i;
}tTermComplex;

tTermComplex a[MAX_TERMS], b[MAX_TERMS], c[MAX_TERMS];
tTermComplex* cptr = c;

void initSparseMat(FILE **fp, tTermComplex Mat[]);
void addSparseMat(tTermComplex a[], tTermComplex b[], tTermComplex* c[]);
void multSparseMat(tTermComplex a[], tTermComplex b[], tTermComplex* c[]);
void printSparseMat(tTermComplex Mat[]);

void main()
{
	FILE *fpA;
	FILE *fpB;
	
	fopen_s(&fpA, "sparseMatA.txt", "r");
	fopen_s(&fpB, "sparseMatB.txt", "r");
	initSparseMat(&fpA, a);
	initSparseMat(&fpB, b);

	puts("Sparse Matrix A:");
	printSparseMat(a);
	puts("Sparse Matrix B:");
	printSparseMat(b);
	puts("Sparse Matrix C = A + B:");
	addSparseMat(a, b, &cptr);
	printSparseMat(c);
	puts("Sparse Matrix C = A * B:");
	multSparseMat(a, b, &cptr);
	printSparseMat(c);


	fclose(fpA);
	fclose(fpB);
	
}

void initSparseMat(FILE **fp, tTermComplex Mat[])
{
	int j=1;
	fscanf_s(*fp, "%d %d %d", &(Mat[0].row), &(Mat[0].col), &(Mat[0].r));
	while(!feof(*fp)){
		fscanf_s(*fp, "%d %d %d %d", &(Mat[j].row), &(Mat[j].col), &(Mat[j].r), &(Mat[j].i));
		j++;
	}
}
void addSparseMat(tTermComplex a[], tTermComplex b[], tTermComplex* c[])
{
	int i=1, j=1, k=1;
	while((a[i].col != NULL) && (b[j].col != NULL)){
		if(a[i].row == b[j].row){
			if(a[i].col ==b[j].col ){
				(*c)[k].col = a[i].col;
				(*c)[k].r = a[i].r + b[j].r;
				(*c)[k].i = a[i].i + b[j].i;
				i++; j++;k++;
			}
			else if(a[i].col < b[j].col){
				(*c)[k].row = a[i].row;
				(*c)[k].col = a[i].col;
				(*c)[k].r = a[i].r;
				(*c)[k].i =  a[i].i;
				i++;k++;
			}
			else{
				(*c)[k].row = b[i].row;
				(*c)[k].col = b[i].col;
				(*c)[k].r = b[i].r;
				(*c)[k].i =  b[i].i;
				j++;k++;
			}
		}
	}
	
}
void multSparseMat(tTermComplex a[], tTermComplex b[], tTermComplex* c[])
{
	int i=1, j=1, k=1, sum = 0;
	if(a[i].row == b[j].col && a[i].col == b[j].row){
		
	}
}

void printSparseMat(tTermComplex Mat[])
{
	int i, j, k=1;
	for(i=0; i<6; i++){
			for(j=0; j<6; j++){
				if(((i+1)==Mat[k].row) && ((j+1)==Mat[k].col)){
					printf("%d+%di ", Mat[k].r, Mat[k].i);
					k++;
				}else
					printf("%-5d    ", 0);			
		}
			puts(" ");
	}
}